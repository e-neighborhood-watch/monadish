module Control.Monad.Shell where

import Control.Monad.Shell.Types.Shell
import Control.Monad.Shell.Types.FilePath
import Control.Monad.Shell.Classes.Path

import Control.Monad
import Control.Monad.State

-- | Changes the current directory
cd :: Path a => a -> Shell ()
cd = makeAbsolute >=> put

-- | Gets absolute path of the current directory
pwd :: Shell AbsolutePath
pwd = get
