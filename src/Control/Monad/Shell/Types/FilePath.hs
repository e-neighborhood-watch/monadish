module Control.Monad.Shell.Types.FilePath where

import Data.ByteString

newtype AbsolutePath = AbsolutePath [ ByteString ]

newtype RelativePath = RelativePath [ ByteString ]

