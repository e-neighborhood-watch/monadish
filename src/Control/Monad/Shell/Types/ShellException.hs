module Control.Monad.Shell.Types.ShellException where

import Control.Monad.Shell.Types.FilePath

data ShellException
  = NoDirectory AbsolutePath
