module Control.Monad.Shell.Types.Shell
  ( Shell
  ) where

import Control.Monad.Shell.Types.FilePath
import Control.Monad.Shell.Types.ShellException

import Control.Monad.State
import Control.Monad.Trans.Except

type Shell a = ExceptT ShellException (StateT AbsolutePath IO) a
