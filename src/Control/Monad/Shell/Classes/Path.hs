module Control.Monad.Shell.Classes.Path
  ( Path (..)
  , systemPath
  ) where

import Control.Monad.Shell.Types.FilePath
import Control.Monad.Shell.Types.Shell

import Data.Word
  ( Word8
  )
import qualified Data.ByteString as ByteString
import Data.ByteString
  ( ByteString
  )
import Control.Monad.State

class Path a where
  makeAbsolute :: a -> Shell AbsolutePath

instance Path AbsolutePath where
  makeAbsolute = return

-- TODO symlinks!
instance Path RelativePath where
  makeAbsolute (RelativePath pathStrings) = do
    (AbsolutePath absolutePathStrings) <- get
    return $ AbsolutePath $ absolutePathStrings ++ pathStrings

-- | Alias for the slash character x2F
slash :: Word8
slash = 47

-- | Convert an absolute path to the sequence of bytes that will be used by the system
systemPath :: AbsolutePath -> ByteString
systemPath (AbsolutePath pathStrings) =
    ByteString.concat $ map (ByteString.cons slash) pathStrings
